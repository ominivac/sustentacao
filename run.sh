docker stop td

docker run -p 8080:8080 \
-e DB_AUTH_DATASOURCE_NAME="tjrndefaultarchetype-db-auth" \
-e DB_AUTH_URL="jdbc:jtds:sqlserver://t-eua.intrajus.tjrn:1433;DatabaseName=TjrnDefaultArchetype" \
-e DB_AUTH_USER="sa" \
-e DB_AUTH_PASSWORD="devsoft@" \
-e DB_APP_DATASOURCE_NAME="tjrndefaultarchetype-db-app" \
-e DB_APP_URL="jdbc:jtds:sqlserver://t-eua.intrajus.tjrn:1433;DatabaseName=TjrnDefaultArchetype" \
-e DB_APP_USER="sa" \
-e DB_APP_PASSWORD="devsoft@" \
-e AUTH_LDAP_HOST="" \
-e AUTH_LDAP_PORT="" \
-e AUTH_LDAP_ROOT="" \
-e AUTH_LDAP_USER="" \
-e AUTH_LDAP_PASSWORD="" \
-e AUTH_LDAP_USERPRINCIPALNAME="" \
-e JAVA_OPTS="-Dlog_level=DEBUG ${JAVA_OPTS}" \
--name td -it --rm tjrn-default


# Caso queira conferir como estao as variaveis da aplicacao dentro do container
# docker exec -it td printenv | grep "^DB_\|^AUTH_\|^JAVA_OPTS" | sort