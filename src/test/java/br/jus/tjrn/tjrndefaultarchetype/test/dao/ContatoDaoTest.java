/**
 * 
 */
package br.jus.tjrn.tjrndefaultarchetype.test.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import br.jus.tjrn.arq.persistence.PageData;
import br.jus.tjrn.tjrndefaultarchetype.dao.ContatoDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.Contato;

/**
 * @author <a href="mailto:misaelbarreto@gmail.com">Misael Barreto</a>
 *
 */
public class ContatoDaoTest {

    private static ApplicationContext ac;
    private static SessionFactory sessionFactory;
    private static Session session;
    
    @BeforeClass
    public static void setUp() {
        ac = new ClassPathXmlApplicationContext(
        "applicationContext.xml");
        
        /*
         * A rotina abaixo simula o OpenSessionInView, evitando assim o problema abaixo ao carregar dados lazy:
         * "org.hibernate.LazyInitializationException: could not initialize proxy - no Session".
         * 
         * Ref: https://forum.hibernate.org/viewtopic.php?t=929167
         */
        sessionFactory = ac.getBean(SessionFactory.class);
        session = SessionFactoryUtils.getSession(sessionFactory, true);
        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));
    }  
    
    @AfterClass
    public static void tearDown() {
      TransactionSynchronizationManager.unbindResource(sessionFactory);
      SessionFactoryUtils.closeSession(session);
    }
    
    @Test
    public void test() {
        ContatoDao contatoDao = ac.getBean(ContatoDao.class);
        PageData<Contato> pageData = contatoDao.findDinamicoComPaginacaoSobDemanda(null, null, 0, 10, null, false);
        
        if (pageData.getCountAll() > 0) {
            System.out.println("Listando os dados");
            System.out.println("------------------------------");
            System.out.println("Total encontrados: " + pageData.getCountAll());
            
            System.out.println("");
            System.out.println("Listando os 10 primeiros...");            
            System.out.println("");
            
            for (Contato contato : pageData.getData()) {
                System.out.println(String.format("Contato de %s: %s (%s) (id:%d).", contato.getPessoa().getNome(), contato.getDescricao(), contato.getTipoContato().getNome(), contato.getId()));
            }
        } else {
            System.out.println("Nenhum registro encontrado");
        }

    }
        
    
}
