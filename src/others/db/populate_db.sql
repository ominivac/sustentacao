USE TjrnDefaultArchetype
GO

/*
=====================================
Cadastrando os dados para autorização
=====================================
*/

-- Inserindo as operações.
INSERT	INTO Operacao
		(codigo, nome, descricao)
SELECT	'OP_TIPO_CONTATO_LER', 'Tipo Contato - Ler', 'Permite a inclusão de um Tipo de Contato' UNION
SELECT	'OP_TIPO_CONTATO_INCLUIR', 'Tipo Contato - Incluir', 'Permite a inclusão de um Tipo de Contato' UNION
SELECT	'OP_TIPO_CONTATO_ALTERAR', 'Tipo Contato - Alterar', 'Permite a alterção de um Tipo de Contato' UNION
SELECT	'OP_TIPO_CONTATO_EXCLUIR', 'Tipo Contato - Excluir', 'Permite a exclusão de um Tipo de Contato' UNION

SELECT	'OP_CONTATO_LER', 'Tipo Contato - Ler', 'Permite a inclusão de um Contato' UNION
SELECT	'OP_CONTATO_INCLUIR', 'Tipo Contato - Incluir', 'Permite a inclusão de um Contato' UNION
SELECT	'OP_CONTATO_ALTERAR', 'Tipo Contato - Alterar', 'Permite a alteração de um Contato' UNION
SELECT	'OP_CONTATO_EXCLUIR', 'Tipo Contato - Excluir', 'Permite a exclusão de um Contato' UNION

SELECT	'OP_PESSOA_LER', 'Tipo PESSOA - Ler', 'Permite a inclusão de um Pessoa' UNION
SELECT	'OP_PESSOA_INCLUIR', 'Tipo PESSOA - Incluir', 'Permite a inclusão de um Pessoa' UNION
SELECT	'OP_PESSOA_ALTERAR', 'Tipo PESSOA - Alterar', 'Permite a alteração de um Pessoa' UNION
SELECT	'OP_PESSOA_EXCLUIR', 'Tipo PESSOA - Excluir', 'Permite a exclusão de um Pessoa'
GO	


-- Inserindo novos usuários.
INSERT	INTO Usuario
		(login, nome, ativo)
SELECT	'dis_consultaad', 'Usuário AD de Sistemas Para Consulta', 1 UNION
SELECT	'f311403', 'Misael B. de Queiroz', 1
GO


/*
Inserindo todas as operações para todos os usuários
caso não exista o vínculo entre os mesmos.
*/
INSERT	INTO UsuarioOperacao
		(usuario_id, operacao_id)	
SELECT	u.id,
		o.id
FROM	Usuario u, Operacao o
WHERE	NOT EXISTS 	(
						SELECT	*
						FROM	UsuarioOperacao uo2
						WHERE	uo2.usuario_id = u.id
								AND uo2.operacao_id = o.id
					)
ORDER	BY u.id, o.id
GO
	

INSERT	INTO TipoUo
		(nome)		
SELECT	'Presidência' UNION
SELECT	'Departamento' UNION
SELECT	'Secretaria' UNION
SELECT	'Divisão' UNION
SELECT	'Órgão'
GO


INSERT	INTO Uo
		(nome, sigla, ativa, tipoUo_id, uoPai_id)
SELECT	nome = 'TJRN',
		sigla = '',
		ativa = 1,
		tipoUo_id = (SELECT id FROM TipoUo WHERE nome = 'Órgão'),
		uoPai_id = NULL
		
INSERT	INTO Uo
		(nome, sigla, ativa, tipoUo_id, uoPai_id)
SELECT	nome = '{TipoUo}',
		sigla = NULL,
		ativa = 1,
		tipoUo_id = (SELECT id FROM TipoUo WHERE nome = 'Presidência'),
		uoPai_id = (SELECT id FROM Uo WHERE nome = 'TJRN')
		
INSERT	INTO Uo
		(nome, sigla, ativa, tipoUo_id, uoPai_id)
SELECT	nome = '{TipoUo} de Informática',
		sigla = 'SETIC',
		ativa = 1,
		tipoUo_id = (SELECT id FROM TipoUo WHERE nome = 'Secretaria'),
		uoPai_id = (SELECT id FROM Uo WHERE nome = '{TipoUo}')	
		
		
INSERT	INTO Uo
		(nome, sigla, ativa, tipoUo_id, uoPai_id)
SELECT	nome = '{TipoUo} de Projetos e Sistemas',
		sigla = 'DPS',
		ativa = 1,
		tipoUo_id = (SELECT id FROM TipoUo WHERE nome = 'Departamento'),
		uoPai_id = (SELECT id FROM Uo WHERE nome = '{TipoUo} de Informática')	

INSERT	INTO Uo
		(nome, sigla, ativa, tipoUo_id, uoPai_id)
SELECT	nome = '{TipoUo} Geral',
		sigla = 'SETIC',
		ativa = 1,
		tipoUo_id = (SELECT id FROM TipoUo WHERE nome = 'Secretaria'),
		uoPai_id = (SELECT id FROM Uo WHERE nome = '{TipoUo}')
			
INSERT	INTO Uo
		(nome, sigla, ativa, tipoUo_id, uoPai_id)
SELECT	nome = '{TipoUo} de Administração',
		sigla = 'SETIC',
		ativa = 1,
		tipoUo_id = (SELECT id FROM TipoUo WHERE nome = 'Secretaria'),
		uoPai_id = (SELECT id FROM Uo WHERE nome = '{TipoUo}')	
GO