/*
====================================
TjrnDefaultArchetype
Script de Criação do Banco de Dados
====================================
*/

/*
USE master
GO

DROP DATABASE TjrnDefaultArchetype
GO

DROP TABLE dbo.Contato
DROP TABLE dbo.Pessoa
DROP TABLE dbo.TipoContato

DROP TABLE dbo.UsuarioOperacao
DROP TABLE dbo.Usuario
DROP TABLE dbo.Operacao

DROP TABLE dbo.Auditoria
*/

CREATE DATABASE TjrnDefaultArchetype
COLLATE Latin1_General_CI_AI
GO

USE TjrnDefaultArchetype
GO

/*
Criando as tabelas para armazenas os dados da aplicação em si.
*/
CREATE TABLE dbo.Pessoa(
	id INT IDENTITY(1,1) NOT NULL,
	foto IMAGE NULL,
	nome VARCHAR(100) NOT NULL,
	nomeAbreviado VARCHAR(50) NULL,
	sexo CHAR(1) NOT NULL,
	cpf VARCHAR(14) NULL,
	dataNascimento DATE NULL,
	dataHoraUltimoRegistro DATETIME NOT NULL,
	loginUsuarioUltimoRegistro VARCHAR(20) NOT NULL,
 CONSTRAINT PK_Pessoa PRIMARY KEY CLUSTERED 
(
	id ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE dbo.Pessoa ADD CONSTRAINT CK_Pessoa_Sexo CHECK ((sexo='F' OR sexo='M'))
GO



CREATE TABLE dbo.TipoContato(
	id INT IDENTITY(1,1) NOT NULL,
	nome VARCHAR(50) NOT NULL,
	regex VARCHAR(50) NULL,
	permiteMaisDeUmRegistroPorPessoa BIT NOT NULL
 CONSTRAINT PK_TipoContato PRIMARY KEY CLUSTERED 
(
	id ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX IX_TipoContato_NomeUnico ON dbo.TipoContato 
(
	nome ASC
)ON [PRIMARY]
GO



CREATE TABLE dbo.Contato(
	id INT IDENTITY(1,1) NOT NULL,
	pessoa_id INT NOT NULL,
	tipoContato_id INT NOT NULL,
	descricao VARCHAR(50) NOT NULL,
CONSTRAINT PK_Contato PRIMARY KEY CLUSTERED 
(
	id ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE dbo.Contato ADD CONSTRAINT FK_Contato_Pessoa FOREIGN KEY(pessoa_id)
REFERENCES dbo.Pessoa(id)
GO

ALTER TABLE dbo.Contato ADD CONSTRAINT FK_Contato_TipoContato FOREIGN KEY(tipoContato_id)
REFERENCES dbo.TipoContato(id)
GO



/*
Criando as tabelas para armazenar os dados de autorização.
*/
CREATE TABLE dbo.Usuario(
	id INT IDENTITY(1,1) NOT NULL,
	login VARCHAR(20) NOT NULL,
	nome VARCHAR(100) NOT NULL,
	ativo BIT NOT NULL,
 CONSTRAINT PK_Usuario PRIMARY KEY CLUSTERED 
(
	id ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX IX_Usuario_LoginUnico ON dbo.Usuario 
(
	login ASC
)ON [PRIMARY]
GO


CREATE TABLE dbo.Operacao(
	id INT IDENTITY(1,1) NOT NULL,
	codigo VARCHAR(50) NOT NULL,
	nome VARCHAR(100) NOT NULL,
	descricao VARCHAR(255) NOT NULL
 CONSTRAINT PK_Operacao PRIMARY KEY CLUSTERED 
(
	id ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX IX_Operacao_CodigoUnico ON dbo.Operacao 
(
	codigo ASC
)ON [PRIMARY]
GO



CREATE TABLE dbo.UsuarioOperacao(
	id INT IDENTITY(1,1) NOT NULL,
	usuario_id INT NOT NULL,
	operacao_id INT NOT NULL,
 CONSTRAINT PK_UsuarioOperacao PRIMARY KEY CLUSTERED 
(
	id ASC
) ON [PRIMARY]
) ON [PRIMARY]


CREATE UNIQUE NONCLUSTERED INDEX IX_UsuarioOperacao_OperacaoUnicaPorUsuario ON dbo.UsuarioOperacao 
(
	usuario_id ASC,
	operacao_id ASC
)ON [PRIMARY]
GO

ALTER TABLE dbo.UsuarioOperacao ADD CONSTRAINT FK_UsuarioOperacao_Operacao FOREIGN KEY(operacao_id)
REFERENCES dbo.Operacao(id)
GO

ALTER TABLE dbo.UsuarioOperacao ADD CONSTRAINT FK_UsuarioOperacao_Usuario FOREIGN KEY(usuario_id)
REFERENCES dbo.Usuario(id)
GO




/*
Criando as tabelas para armazenar os dados de auditoria.
*/
CREATE TABLE dbo.Auditoria(
	id INT IDENTITY(1,1) NOT NULL,
	acao VARCHAR(100) NOT NULL,
	descricao TEXT NOT NULL,
	entidade VARCHAR(100) NULL,
	entidade_id INT NULL,
	dataHora DATETIME NOT NULL,
	aplicacao_codigo VARCHAR(50) NOT NULL,
	usuario_login VARCHAR(20) NOT NULL,
 CONSTRAINT PK_AuditoriaDeOperacao PRIMARY KEY CLUSTERED 
(
	id ASC
) ON [PRIMARY]
) ON [PRIMARY] --TEXTIMAGE_ON [PRIMARY]
GO


CREATE TABLE dbo.TipoUo(
	id INT IDENTITY(1,1) NOT NULL,
	nome VARCHAR(50) NOT NULL,
 CONSTRAINT PK_TipoUo PRIMARY KEY CLUSTERED 
(
	id ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE dbo.Uo(
	id INT IDENTITY(1,1) NOT NULL,
	nome VARCHAR(100) NOT NULL,
	sigla VARCHAR(20) NULL,
	ativa BIT NOT NULL,
	tipoUo_id INT NOT NULL,
	uoPai_id INT NULL,
 CONSTRAINT PK_Uo PRIMARY KEY CLUSTERED 
(
	id ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE dbo.Uo  WITH CHECK ADD  CONSTRAINT FK_Uo_TipoUo FOREIGN KEY(tipoUo_id)
REFERENCES dbo.TipoUo (id)
GO

