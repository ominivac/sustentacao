package $pkgDao;

import org.springframework.stereotype.Repository;

import br.jus.tjrn.arq.persistence.hibernate.HibernateTemplateCrudDao;
import ${pkgModel}.${table.name};

@Repository
public class ${table.name}DaoImpl extends HibernateTemplateCrudDao<${table.name}> implements ${table.name}Dao {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
}
