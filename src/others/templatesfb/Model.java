package $pkgModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.jus.tjrn.arq.persistence.Persistent;
import br.jus.tjrn.arq.persistence.PersistentUtil;

$table.imports

@Entity
@Table(name="$table.name")
public class $table.name implements Persistent {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
#foreach( $column in $table.columns )
    $column.annotations
    private $column.type $column.atributeName;
    
#end


#foreach( $column in $table.columns )
    $column.getter
    $column.setter
#end

    @Override
    @Transient
    public String getLabel() {
        return null;
    }
    
    @Override
    @Transient
    public String getEntityLabel() {
        return "$table.name";
    }    
    
    
    /*
     * TODO: Implementar hashCode, equals e toString.
     * No toString, acrescentar comparacao por Id, e nessa
     * comparacao remova o trecho...
     * 
     *  Entity other = (Entity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
     * 
     * ... e acrescente o trecho logo no início, após
     * o trecho "Entity other...":
     *  
     *  Entity other = (Entity) obj;
        if (PersistentUtil.hasSameId(this, other)) {
            return true;
        }
     * 
     */

}
