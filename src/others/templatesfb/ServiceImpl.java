package $pkgService;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.jus.tjrn.arq.security.spring.RoleGroup;
import br.jus.tjrn.arq.service.CrudServiceImpl;
import ${pkgDao}.${table.name}Dao;
import ${pkgModel}.${table.name};

@Service
@RoleGroup(name="$table.name.toUpperCase()")
public class ${table.name}ServiceImpl extends CrudServiceImpl<${table.name}, ${table.name}Dao> implements ${table.name}Service{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
    @Inject
    public ${table.name}ServiceImpl(${table.name}Dao dao) {
        super(dao);
    }

}
