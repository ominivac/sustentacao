package $pkgBean;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.jus.tjrn.arq.jsf.controls.AbstractCrudBean;
import ${pkgModel}.${table.name};
import ${pkgService}.${table.name}Service;

@Controller
@Scope("view")
public class ${table.name}Bean extends AbstractCrudBean<$table.name, ${table.name}Service> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
#foreach( $column in $table.columns )
#if($!column.foreignKey)
    private ${column.type}Service ${column.atributeName}Service;
#end
#end

#foreach( $column in $table.columns )
#if($!column.foreignKey)
    private List<${column.type}> ${column.atributeName}List;
#end
#end

@Inject
public ${table.name}Bean(
                            ${table.name}Service service
                            #foreach( $column in $table.columns )
                            #if($!column.foreignKey) 
                                ,${column.type}Service ${column.atributeName}Service 
                            #end
                            #end
        ) {
    super(service);
    #foreach( $column in $table.columns )
    #if($!column.foreignKey) 
        this.${column.atributeName}Service = ${column.atributeName}Service; 
    #end
    #end    
}

/**
 * Método que pode ser utilizado para carregar dados de serviços auxiliares.
 */
@Override
public void onInit() {
    #foreach( $column in $table.columns )
        #if($!column.foreignKey)     
            set${column.type}List(this.${column.atributeName}Service.findAll());
        #end
    #end
}

#foreach( $column in $table.columns )
#if($!column.foreignKey)
    public List<${column.type}> get${column.type}List() {
        return ${column.atributeName}List;
    }
    
    public void set${column.type}List(List<${column.type}> ${column.atributeName}List) {
        this.${column.atributeName}List = ${column.atributeName}List;
    }
    
#end
#end

}