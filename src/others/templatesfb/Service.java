package $pkgService;

import br.jus.tjrn.arq.service.CrudService;
import ${pkgModel}.${table.name};

public interface ${table.name}Service extends CrudService<${table.name}> {   
}
