package $pkgDao;

import br.jus.tjrn.arq.persistence.CrudDao;
import ${pkgModel}.${table.name};

public interface ${table.name}Dao extends CrudDao<${table.name}>{
}
