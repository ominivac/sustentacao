ECHO ----------------------------------
ECHO Instalando archetype local do TJRN
ECHO ----------------------------------

cd..\..
call mvn clean eclipse:clean
call mvn archetype:create-from-project
cd target\generated-sources\archetype
call mvn install

ECHO 
ECHO 
ECHO --------------------------------
ECHO Instalacao realizada com sucesso
ECHO Duvidas: Acesse http://s-mexico.intrajus.tjrn:4000/projects/projetopadrao/wiki/Default_Archetype
ECHO --------------------------------

PAUSE