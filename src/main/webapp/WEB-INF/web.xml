<?xml version="1.0" encoding="UTF-8"?>
<!--
    * Copyright (c) 2011, Tribunal de Justiça do RN.
    * Todos os direitos reservados.
-->
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://java.sun.com/xml/ns/javaee"
    xmlns:web="http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
    xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"
    version="2.5">

    <!--
        Usa assinador? Caso esteja tendo problemas para usar assinador applet java, tente fazer o seguinte
        ajuste na tag "Context" do context.xml do seu tomcat (apache-tomcat/conf/context.xml).
        <Context useHttpOnly="false">
    -->

    <!-- Configurações gerais J2EE -->
    <display-name>Archetype</display-name>
    <welcome-file-list>
        <welcome-file>f/index.xhtml</welcome-file>
    </welcome-file-list>

    <session-config>
        <session-timeout>15</session-timeout>
    </session-config>


    <!-- Configurações JSF -->
    <servlet>
        <servlet-name>Faces Servlet</servlet-name>
        <servlet-class>javax.faces.webapp.FacesServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>
    <servlet-mapping>
        <servlet-name>Faces Servlet</servlet-name>
        <url-pattern>/f/*</url-pattern>
        <url-pattern>/faces/*</url-pattern>
    </servlet-mapping>
    <context-param>
        <param-name>javax.faces.PROJECT_STAGE</param-name>
        <param-value>Development</param-value>
    </context-param>
    <context-param>
        <param-name>javax.faces.DEFAULT_SUFFIX</param-name>
        <param-value>.xhtml</param-value>
    </context-param>
    <context-param>
        <param-name>primefaces.THEME</param-name>
        <param-value>none</param-value>
    </context-param>


    <!-- Listeners do Spring -->
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
    <listener>
        <listener-class>org.springframework.web.context.request.RequestContextListener</listener-class>
    </listener>

    <!-- Filter de Autenticação do Spring -->
    <filter>
        <filter-name>springSecurityFilterChain</filter-name>
        <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>springSecurityFilterChain</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <!--
        ===================================
        Open Session in View Spring Filter
        ===================================
        Referências:
        http://grepcode.com/file/repo1.maven.org/maven2/org.springframework/spring-orm/3.0.1.RELEASE/org/springframework/orm/hibernate3/support/OpenSessionInViewFilter.java

        Valor default dos parâmetros de inicialização:
        * sessionFactoryBeanName = sessionFactory
        * singleSession = true
        * flushMode = MANUAL
    -->
    <filter>
        <filter-name>openSessionInViewFilter</filter-name>
        <filter-class>org.springframework.orm.hibernate3.support.OpenSessionInViewFilter</filter-class>
        <init-param>
            <param-name>singleSession</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>openSessionInViewFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>


    <context-param>
        <param-name>primefaces.UPLOADER</param-name>
        <!-- 
            Valores possíveis: auto|native|commons
            
            auto: Dependendo de algumas condições é selecionado o native ou o 
            commons.
            
            native: Durante testes vi que não respeita o thresholdSize, ou seja,
            sempre tenta escrever o arquivo na pasta temporária que fica dentro da 
            work do Tomcat.
            Ex: apache-tomcat-7.0.22\work\Catalina\localhost\myproject
            Essa configuração geralmente acaba acarretando o problema do arquivo que
            está sendo anexado visualmente "sumir", e consequentemente, acabar não 
            sendo anexado. Isso ocorre geralmente por falta de permissão de escrita,
            mas as vezes, mesmo com a devida permisssão e conseguindo escrever algumas
            vezes, depois de um temppo o tomcat acaba se "perdendo" e gerando esse
            transtorno. 
            
            commons: Ele respeita o thresholdSize. Os arquivos serão salvos no diretório
            definido em "uploadDirectory". Caso não seja definido, os arquivos serão
            salvos no diretório temporário retornado por "System.getProperty("java.io.tmpdir")".
            Essa é a opção recomendada!!!
         -->
        <!-- 
        Ref:
        https://blog.algaworks.com/primefaces-fileupload/3/
        https://www.primefaces.org/docs/guide/primefaces_user_guide_5_2.pdf  (Página 210)
         -->
        <param-value>commons</param-value>
    </context-param>
    <!--
        ===================================
        Filtro do File Upload (Prime Faces)
        ===================================
        Obs: A documentação do Prime Faces sugere que este filtro seja um dos
        primeiros da lista de filtros.
    -->
    <filter>
        <filter-name>PrimeFaces FileUpload Filter</filter-name>
        <filter-class>org.primefaces.webapp.filter.FileUploadFilter</filter-class>
        <init-param>
            <!-- 
                Tamanho em Bytes:
                1048576 Bytes = 1024 KB = 1MB
                2097152 Bytes = 2048 KB = 2MB
                3145728 Bytes = 3072 KB = 3MB
                4194304 Bytes = 4096 KB = 4MB
                5242880 Bytes = 5120 KB = 5MB
                10485760 Bytes = 10240 KB = 10MB
            -->
            <!--
            	Se o arquivo foi menor ou igual ao tamanho aqui especificado,
            	tudo será feito em memória, caso contrário, ele será persistido
            	temporariamente no disco.
           	-->
            <param-name>thresholdSize</param-name>
            <param-value>5242880</param-value>
        </init-param>
        
        <!--  Por padrão, deixe este parâmetro comentado! -->
        <!-- 
		<init-param>
		    <param-name>uploadDirectory</param-name>
		    <param-value>C:\Temp\</param-value>
		</init-param>
		-->
    </filter>
    <filter-mapping>
        <filter-name>PrimeFaces FileUpload Filter</filter-name>
        <servlet-name>Faces Servlet</servlet-name>
    </filter-mapping>


    <!-- Necessário após a atualização do primefaces para versão 3.3.1 -->
    <filter>
        <filter-name>characterEncodingFilter</filter-name>
        <filter-class>br.jus.tjrn.arq.jsf.utils.CharacterEncodingFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>characterEncodingFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>


    <!-- Error pages -->
    <error-page>
        <exception-type>javax.faces.application.ViewExpiredException</exception-type>
        <location>/f/specialpages/viewexpired_errorpage.xhtml</location>
    </error-page>
    <error-page>
        <error-code>404</error-code>
        <location>/f/specialpages/404.xhtml</location>
    </error-page>

    <!-- misc -->
    <mime-mapping>
        <extension>ico</extension>
        <mime-type>image/vnd.microsoft.icon</mime-type>
    </mime-mapping>

    <context-param>
        <param-name>javax.faces.INTERPRET_EMPTY_STRING_SUBMITTED_VALUES_AS_NULL</param-name>
        <param-value>true</param-value>
    </context-param>

</web-app>