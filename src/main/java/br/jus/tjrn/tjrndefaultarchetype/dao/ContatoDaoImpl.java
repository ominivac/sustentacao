package br.jus.tjrn.tjrndefaultarchetype.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.jus.tjrn.arq.persistence.PageData;
import br.jus.tjrn.arq.persistence.PageDataImpl;
import br.jus.tjrn.arq.persistence.hibernate.HibernateTemplateCrudDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.Contato;
import br.jus.tjrn.tjrndefaultarchetype.domain.Pessoa;

@Repository
public class ContatoDaoImpl extends HibernateTemplateCrudDao<Contato> implements ContatoDao {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    public List<Contato> findByPessoaViaCriteria(Pessoa pessoa) {
        List<Contato> result = new ArrayList<Contato>();

        DetachedCriteria criteria = DetachedCriteria.forClass(Contato.class);
        criteria.add(Restrictions.eq("pessoa", pessoa));
        criteria.addOrder(Order.asc("descricao"));
        result.addAll(ht.findByCriteria(criteria));

        return result;
    }

    @Override
    public List<Contato> findByPessoaViaHql(Pessoa pessoa) {
        List<Contato> result = new ArrayList<Contato>();

        String hql = "from Contato c where c.pessoa = :pessoa order by descricao";
        result = ht.findByNamedParam(hql, "pessoa", pessoa);
        
        // Exemplo de como passar vários parâmetros.
        // ht.findByNamedParam("hql",
        //                      new String[] {"param1", "param2", "param3"},
        //                      new Object[] {obj1, obj2, obj3});

        return result;
    }

    /*
     * Exemplo de método no qual o usuário, por alguma necessidade, necessitou
     * criar uma nova sessão. Nesse caso, é de responsabilidade do usuário fazer
     * a gestão dessa sessão, assim como de alguma transação que tiver sendo
     * executada.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Contato> findByPessoaUsandoNovaSessao(Pessoa pessoa) {
        List<Contato> result = new ArrayList<Contato>();

        Session session = ht.getSessionFactory().openSession();
        try {
            Criteria criteria = session.createCriteria(Contato.class);
            criteria.add(Restrictions.eq("pessoa", pessoa));
            criteria.addOrder(Order.asc("descricao"));
            result.addAll(criteria.list());
        } finally {
            session.close();
        }

        return result;
    }

    /*
     * Exemplo de método no qual o usuário, por alguma necessidade, necessitou
     * obter a sessão corrente. Obs: Essa sessão corrente só estará disponível
     * num ambiente transacional, portanto para poder utilizá-la é necessário o
     * uso da anotação @Transactional, caso contrário a exceção a seguir será
     * lançada: "No Hibernate Session bound to thread, and configuration does
     * not allow creation of non-transactional one here"
     * 
     * Utilizando a sessão corrente o usuário não preciosa se precupar em
     * gerenciar a sessão e tão pouco a transação.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Contato> findByPessoaUsandoSessaoCorrente(Pessoa pessoa) {
        List<Contato> result = new ArrayList<Contato>();

        Session session = ht.getSessionFactory().getCurrentSession();
        Criteria criteria = session.createCriteria(Contato.class);
        criteria.add(Restrictions.eq("pessoa", pessoa));
        criteria.addOrder(Order.asc("descricao"));
        result.addAll(criteria.list());

        return result;
    }    
    
    @Override
    @Transactional(readOnly = true)
    public PageData<Contato> findDinamicoComPaginacaoSobDemanda(String nomePessoa, String nomeTipoContato,
            int first, int pageSize, String sortField, boolean sortOrder) {
        Long countAll = 0L;
        List<Contato> data = new ArrayList<Contato>();
        Map<String, Object[]> params = new HashMap<String, Object[]>();
        
        String hql =    " select  {select} " +
                        " from    Contato c " +
                        " inner   join c.pessoa p " +
                        " inner   join c.tipoContato tc " +
                        " {where} " +
                        " {order_by} ";        
        
        if (nomePessoa != null && nomePessoa.length() > 0) {
            params.put("nomePessoa",
                        new Object[] {" ### p.nome like :nomePessoa", "%"+nomePessoa+"%"});
        }
        if (nomeTipoContato != null && nomeTipoContato.length() > 0) {
            params.put("nomeTipoContato",
                        new Object[] {" ### tc.nome like :nomeTipoContato", "%"+nomeTipoContato+"%"});
        }
        
        // Adicionando os filtros que serão usados no "where".
        String where = "";
        for (Map.Entry<String, Object[]> entry: params.entrySet()) {
            where += entry.getValue()[0];
        }
        where = where.replace("###", "where");
        where = where.replaceAll("###", "and");
        
        hql = hql.replace("{where}", where);
        Session session = ht.getSessionFactory().getCurrentSession();
        
        // Montando a consulta para trazer o quantitativo geral
        Query queryCountAll = session.createQuery(hql.replace("{select}", "count(c)").replace("{order_by}", ""));
        // Montando a consulta para trazer os dados em si.
        Query queryData = session.createQuery(hql.replace("{select}", "c").replace("{order_by}", "order by p.nome, tc.nome"));

        // Adicionando os valores dos filtros.
        for (Map.Entry<String, Object[]> entry: params.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue()[1];
            
            queryCountAll.setParameter(key, value);
            queryData.setParameter(key, value);
        }
        
        if (first > 0) {
            queryData.setFirstResult(first);
        }
        if (pageSize > 0) {
            queryData.setMaxResults(pageSize);
        }
        if (sortField != null && sortField.length() > 0) {
            //TODO: Não implementado
        }

        countAll = (Long)queryCountAll.uniqueResult();
        data = queryData.list();
        return new PageDataImpl<Contato>(countAll.intValue(), data);
    }
    
}
