package br.jus.tjrn.tjrndefaultarchetype.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import br.jus.tjrn.arq.persistence.PageData;
import br.jus.tjrn.arq.persistence.PageDataImpl;
import br.jus.tjrn.arq.persistence.hibernate.HibernateTemplateCrudDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.Uo;

@Repository
public class UoDaoImpl extends HibernateTemplateCrudDao<Uo> implements UoDao {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
    
    /*
     * Este método tem de ser melhorado, para contemplar os outros atributos
     * da entidade Uo. Pretendo montar um método que já monte o HQL, assim
     * como tem usando Criteria (e que hoje ainda é privado).
     * HibernateTemplateCrudDao - mountCriteriaForFindByAttributes 
     */
    @Override
    public PageData<Uo> findPageDataByAttributes(Uo entity, int firstResult,
            int pageSize, String sortField, Boolean sortOrder) {
        List<Uo> result = new ArrayList<Uo>();
        
        String hql = " select uo " +
                     " from   Uo uo " +
                     " inner  join uo.tipoUo tuo ";
   
        String hqlCount = " select uo from Uo ";

        if (entity.getNome() != null && entity.getNome().trim().length()>0) {
            hql = hql + String.format(
                                        " ### replace(%s, %s, %s) like %s ",
                                        "uo.nome", "'{TipoUo}'", "tuo.nome", "'%"+entity.getNome()+"%'"
                                     );
        }
        
        
//        for(Map.Entry<String, Object> entry : params.entrySet()) {
//            if (entry.getValue() instanceof String) {
//                hql = hql + String.format(
//                                            " ### replace(%s, %s, %s) like %s ",
//                                            entry.getKey(), entry.getKey(), "'{TipoUo}'", "tuo.descricao"
//                                         );
//            } else {
//                hql = hql + " ### " + entry.getKey() + " = :" + entry.getKey();
//            }
//        }
        hql = hql.replace("###", "where");
        hql = hql.replaceAll("###", "and");
        
        // result = ht.findByNamedParam(hql, params.keySet().toArray(new String[0]), params.values().toArray());
        result = ht.find(hql);
        return new PageDataImpl<Uo>(result.size(), result);

    }        
}
