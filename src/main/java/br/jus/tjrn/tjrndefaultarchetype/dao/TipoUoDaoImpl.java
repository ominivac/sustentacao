package br.jus.tjrn.tjrndefaultarchetype.dao;

import org.springframework.stereotype.Repository;

import br.jus.tjrn.arq.persistence.hibernate.HibernateTemplateCrudDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoUo;

@Repository
public class TipoUoDaoImpl extends HibernateTemplateCrudDao<TipoUo> implements TipoUoDao {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
}
