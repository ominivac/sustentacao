package br.jus.tjrn.tjrndefaultarchetype.dao;

import java.util.List;

import br.jus.tjrn.arq.persistence.CrudDao;
import br.jus.tjrn.arq.persistence.PageData;
import br.jus.tjrn.tjrndefaultarchetype.domain.Contato;
import br.jus.tjrn.tjrndefaultarchetype.domain.Pessoa;

public interface ContatoDao extends CrudDao<Contato>{
    
    List<Contato> findByPessoaViaCriteria(Pessoa pessoa);
    List<Contato> findByPessoaViaHql(Pessoa pessoa);
    List<Contato> findByPessoaUsandoNovaSessao(Pessoa pessoa);
    List<Contato> findByPessoaUsandoSessaoCorrente(Pessoa pessoa);
    PageData<Contato> findDinamicoComPaginacaoSobDemanda(
            String nomePessoa, String nomeTipoContato, int first, int pageSize,
            String sortField, boolean sortOrder);
}
