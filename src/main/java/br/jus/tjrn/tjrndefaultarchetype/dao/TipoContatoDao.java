package br.jus.tjrn.tjrndefaultarchetype.dao;

import java.util.List;

import br.jus.tjrn.arq.persistence.CrudDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoContato;

public interface TipoContatoDao extends CrudDao<TipoContato>{
    
    public abstract List<TipoContato> findByNomeExistentePorPessoa(String nome, Integer pessoaId); 
}
