package br.jus.tjrn.tjrndefaultarchetype.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.jus.tjrn.arq.persistence.hibernate.HibernateTemplateCrudDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoContato;

@Repository
public class TipoContatoDaoImpl extends HibernateTemplateCrudDao<TipoContato> implements TipoContatoDao {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    public List<TipoContato> findByNomeExistentePorPessoa(String nome, Integer pessoaId) {
        nome = "%" + nome + "%";
        
        String hql =    " select  tc " + 
                        " from    TipoContato tc " +
                        " where   tc.nome like :nome " +
                        "         and exists  ( " +
                        "                        select  c" +
                        "                        from    Contato c " +
                        "                        where   c.pessoa.id = :pessoa_id " +
                        "                                and tc.id = c.tipoContato.id " +
                        "                      )";
        
        
        return ht.findByNamedParam(hql,
                new String[]{"nome", "pessoa_id"},
                new Object[]{nome, pessoaId});
    }    
    
}
