package br.jus.tjrn.tjrndefaultarchetype.dao;

import org.springframework.stereotype.Repository;

import br.jus.tjrn.arq.persistence.hibernate.HibernateTemplateCrudDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.Pessoa;

@Repository
public class PessoaDaoImpl extends HibernateTemplateCrudDao<Pessoa> implements PessoaDao {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
}
