package br.jus.tjrn.tjrndefaultarchetype.dao;

import br.jus.tjrn.arq.persistence.CrudDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoUo;

public interface TipoUoDao extends CrudDao<TipoUo>{
}
