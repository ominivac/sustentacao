/**
 * 
 */
package br.jus.tjrn.tjrndefaultarchetype.dao.utils;

import org.hibernate.dialect.SQLServer2008Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;

/**
 * @author misael
 *
 * Referências:
 * https://hibernate.atlassian.net/secure/attachment/12480/SolidDialect.java
 * https://marzapol.wordpress.com/2013/07/23/custom-function-is-hibernate/
 * https://docs.jboss.org/hibernate/orm/3.3/reference/pt-BR/html/querycriteria.html
 */
public class DefaultSqlServer2008Dialect extends SQLServer2008Dialect {

    public DefaultSqlServer2008Dialect() {
        registerFunction( "replace", new StandardSQLFunction( "replace") );
    }
}
