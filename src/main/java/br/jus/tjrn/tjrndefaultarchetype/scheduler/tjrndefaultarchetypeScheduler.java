package br.jus.tjrn.tjrndefaultarchetype.scheduler;

import java.text.ParseException;

import javax.inject.Inject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import br.jus.tjrn.arq.exception.ServiceBusinessException;
import br.jus.tjrn.arq.security.jaxws.handler.authenticator.CredentialAuthenticator;
import br.jus.tjrn.tjrndefaultarchetype.domain.Pessoa;
import br.jus.tjrn.tjrndefaultarchetype.service.PessoaService;

import com.sun.istack.logging.Logger;

/**
 * UC- TEMPLATE PARA BATCH COM AUTENTICACAO EM MEMORIA
 * 
 * @author Joseilton Silva - v000406 - Lampp-It
 * @throws ParseException
 * @since 23/03/2020
 * @descrição Este caso de uso possibilita recuperar das informações de pessoa.
 */

/**
 * NOTA:
 * 
 * 1) Criar um arquivo .xml para incluir as configurações de autenticação. 
 * Para este exemplo foi criado um arquivo authApplicationContext.xml dentro da pasta commons.
 * 
 * 2) Incluir no beans do arquivo applicationContext.xml os itens abaixo:
      <beans xmlns="http://www.springframework.org/schema/beans"
        ...
        xmlns:task="http://www.springframework.org/schema/task"
        http://www.springframework.org/schema/task
        http://www.springframework.org/schema/task/spring-task-3.1.xsd
        ...
        ">
    
 * 3) Incluir no arquivo applicationContext.xml as configurações abaixo:
      <!-- ======================= Implementacao Scheduler ======================= -->
      <bean id="tjrndefaultarchetypeScheduler" class="br.jus.tjrn.tjrndefaultarchetype.scheduler"></bean>
      <task:scheduled-tasks>
        <!-- Format Cron Expression: <segundo> <minuto> <hora> <dia-do-mes> <mes> <ano>. -->
        <task:scheduled ref="tjrndefaultarchetypeScheduler" method="run" cron="00 08 13 ? * * " />
      </task:scheduled-tasks>
 * 
 *
 */


@Controller
@Scope("view")
public class tjrndefaultarchetypeScheduler {

    static ApplicationContext ac;

    private Logger log = Logger.getLogger(getClass());

    @Inject
    private PessoaService pessoaService;

    @Transactional(rollbackFor = Exception.class)
    public void run() throws ServiceBusinessException, ParseException {
        authenticateBatchInMemory();
        
        /**
         * Rotina que o batch deve executar
         */
        Pessoa p = new Pessoa();
        p.setId(1);
        
        Pessoa result = pessoaService.findByAttributesUniqueResult(p);
        
        if(result != null){
            System.out.println();
            System.out.println("###################");
            System.out.println(result.getNome());
            System.out.println(result.getCpf());
            System.out.println(result.getSexo());
            System.out.println("###################");
        }
        
        
        clearAuthentication();
    }

    /**
     * Método responsável por realizar a autenticação em memória. O objetivo é
     * resolver o problema da ausência de usuário quando se precisa realizar uma
     * atividade automática.
     */
    private static void authenticateBatchInMemory() {

        ac = new ClassPathXmlApplicationContext("authApplicationContext.xml");

        CredentialAuthenticator auth = ac
                .getBean(CredentialAuthenticator.class);
        auth.authenticate("batch", "admin");

        // System.out.println(ac.getClass().getName());
        // System.out.println(auth.toString());
    }

    private static void clearAuthentication() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

}
