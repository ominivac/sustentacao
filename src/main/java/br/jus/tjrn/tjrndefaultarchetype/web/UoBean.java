package br.jus.tjrn.tjrndefaultarchetype.web;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.jus.tjrn.arq.jsf.controls.AbstractCrudBean;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoUo;
import br.jus.tjrn.tjrndefaultarchetype.domain.Uo;
import br.jus.tjrn.tjrndefaultarchetype.service.TipoUoService;
import br.jus.tjrn.tjrndefaultarchetype.service.UoService;

@Controller
@Scope("view")
public class UoBean extends AbstractCrudBean<Uo, UoService> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private TipoUoService tipoUoService;

    private List<TipoUo> tipoUoList;

    @Inject
    public UoBean(UoService service, TipoUoService tipoUoService) {
        super(service);
        this.tipoUoService = tipoUoService;
    }

    /**
     * Método que pode ser utilizado para carregar dados de serviços auxiliares.
     */
    @Override
    public void onInit() {
        setTipoUoList(this.tipoUoService.findAll());
    }

    public List<TipoUo> getTipoUoList() {
        return tipoUoList;
    }

    public void setTipoUoList(List<TipoUo> tipoUoList) {
        this.tipoUoList = tipoUoList;
    }

}