package br.jus.tjrn.tjrndefaultarchetype.web;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.primefaces.event.SelectEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.jus.tjrn.arq.jsf.controls.AbstractBean;
import br.jus.tjrn.tjrndefaultarchetype.domain.Contato;
import br.jus.tjrn.tjrndefaultarchetype.domain.Pessoa;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoContato;
import br.jus.tjrn.tjrndefaultarchetype.service.ContatoService;
import br.jus.tjrn.tjrndefaultarchetype.service.PessoaService;
import br.jus.tjrn.tjrndefaultarchetype.service.TipoContatoService;

@Controller
@Scope("view")
public class AgendaBean extends AbstractBean {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private PessoaService pessoaService;
    private TipoContatoService tipoContatoService;
    private ContatoService contatoService;

    private Pessoa pessoa;
    private TipoContato tipoContato;
    private List<Contato> contatoList;

    @Inject
    public AgendaBean(ContatoService service, PessoaService pessoaService,
            TipoContatoService tipoContatoService, ContatoService contatoService) {
        this.pessoaService = pessoaService;
        this.tipoContatoService = tipoContatoService;
        this.contatoService = contatoService;
    }

    /**
     * Método que pode ser utilizado para carregar dados de serviços auxiliares.
     */
    @Override
    public void onInit() {
        setPessoa(new Pessoa());
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public List<Contato> getContatoList() {
        return contatoList;
    }

    public void setContatoList(List<Contato> contatoList) {
        this.contatoList = contatoList;
    }

    public List<Pessoa> findPessoas(String nome) {
        Pessoa p = new Pessoa();
        p.setNome(nome);

        return pessoaService.findByAttributes(p);
    }

    public void selecionarPessoa(SelectEvent event) {
        setPessoa((Pessoa) event.getObject());
        setTipoContato(new TipoContato());
        setContatoList(new ArrayList<Contato>());
    }

    public List<TipoContato> findTiposContatos(String nome) {
        if (existePessoaSelecionada()) {
            return tipoContatoService.findByNomeExistentePorPessoa(nome,
                    this.pessoa.getId());
        } else {
            return new ArrayList<TipoContato>();
        }
    }
    
    public void selecionarTipoContato(SelectEvent event) {
        setTipoContato((TipoContato) event.getObject());
        
        /*
         * Com base nos parâmetros fornecidos, será realizada
         * uma busca pelos contatos existentes para a pessoa
         * e tipo de contato em questão.
         */
        Contato c = new Contato();
        c.setPessoa(getPessoa());
        c.setTipoContato(getTipoContato());
        
        setContatoList(contatoService.findByAttributes(c));
    }

    public Boolean existePessoaSelecionada() {
        Boolean existePessoaSelecionada = (this.pessoa != null && this.pessoa
                .getId() != null);
        if (!existePessoaSelecionada) {
            addError("Antes de buscar os tipos de contato se faz necessário identificar a pessoa");
        }

        return existePessoaSelecionada;
    }

    public void limparPesquisa() {
        setPessoa(new Pessoa());
        setTipoContato(new TipoContato());
        setContatoList(new ArrayList<Contato>());
    }

}