package br.jus.tjrn.tjrndefaultarchetype.web;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.jus.tjrn.arq.jsf.controls.AbstractCrudBean;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoUo;
import br.jus.tjrn.tjrndefaultarchetype.service.TipoUoService;

@Controller
@Scope("view")
public class TipoUoBean extends AbstractCrudBean<TipoUo, TipoUoService> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Inject
    public TipoUoBean(TipoUoService service) {
        super(service);
    }

    /**
     * Método que pode ser utilizado para carregar dados de serviços auxiliares.
     */
    @Override
    public void onInit() {
    }

}