package br.jus.tjrn.tjrndefaultarchetype.web;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.jus.tjrn.arq.jsf.controls.AbstractCrudBean;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoContato;
import br.jus.tjrn.tjrndefaultarchetype.service.TipoContatoService;

@Controller
@Scope("view")
public class TipoContatoBean extends
        AbstractCrudBean<TipoContato, TipoContatoService> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Inject
    public TipoContatoBean(TipoContatoService service) {
        super(service);
    }

    /**
     * Método que pode ser utilizado para carregar dados de serviços auxiliares.
     */
    @Override
    public void onInit() {
    }

}