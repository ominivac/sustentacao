package br.jus.tjrn.tjrndefaultarchetype.web;

import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.jus.tjrn.arq.jsf.controls.AbstractCrudBean;
import br.jus.tjrn.arq.jsf.utils.FacesUtil;
import br.jus.tjrn.tjrndefaultarchetype.domain.Contato;
import br.jus.tjrn.tjrndefaultarchetype.domain.Pessoa;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoContato;
import br.jus.tjrn.tjrndefaultarchetype.service.ContatoService;
import br.jus.tjrn.tjrndefaultarchetype.service.PessoaService;
import br.jus.tjrn.tjrndefaultarchetype.service.TipoContatoService;

@Controller
@Scope("view")
public class ContatoBean extends AbstractCrudBean<Contato, ContatoService> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private static final String CONTATO_BEAN_PESSOA_TO_LOAD_EXAMPLE_A = "CONTATO_BEAN_PESSOA_TO_LOAD_EXAMPLE_A";
    
    private PessoaService pessoaService;
    private TipoContatoService tipoContatoService;

    private List<Pessoa> pessoaList;
    private List<TipoContato> tipoContatoList;
    
    private Integer pessoaToLoadExampleC;
   
    @Inject
    public ContatoBean(ContatoService service, PessoaService pessoaService,
            TipoContatoService tipoContatoService) {
        super(service);
        this.pessoaService = pessoaService;
        this.tipoContatoService = tipoContatoService;
    }

    /**
     * Método que pode ser utilizado para carregar dados de serviços auxiliares.
     */
    @Override
    public void onInit() {
        setPessoaList(this.pessoaService.findAll());
        setTipoContatoList(this.tipoContatoService.findAll());
    }
    
    @Override
    protected void onAfterLoadInsertMode() {
        /*
         * =================================================
         * Passando parâmetro entre ManagedBeans - Exemplo A
         * =================================================
         * */
        Object pessoaToLoadExampleA = FacesUtil.getSessionAttribute(CONTATO_BEAN_PESSOA_TO_LOAD_EXAMPLE_A);
        if (pessoaToLoadExampleA != null) {
            getEntity().setPessoa((Pessoa) pessoaToLoadExampleA);
            FacesUtil.getSession().removeAttribute(CONTATO_BEAN_PESSOA_TO_LOAD_EXAMPLE_A);
        }        
        
        /*
         * =================================================
         * Passando parâmetro entre ManagedBeans - Exemplo B
         * =================================================
         * Ex: http://localhost:8180/tjrn-default/f/pages/contato/contato.xhtml?pessoaToLoadExampleB=1
         * */
        String pessoaToLoadExampleB = FacesUtil.getRequest().getParameter("pessoaToLoadExampleB");
        if (pessoaToLoadExampleB != null) {
            getEntity().setPessoa(pessoaService.findById(Integer.valueOf(pessoaToLoadExampleB)));
        }
    }
    

    public List<Pessoa> getPessoaList() {
        return pessoaList;
    }

    public void setPessoaList(List<Pessoa> pessoaList) {
        this.pessoaList = pessoaList;
    }

    public List<TipoContato> getTipoContatoList() {
        return tipoContatoList;
    }

    public void setTipoContatoList(List<TipoContato> tipoContatoList) {
        this.tipoContatoList = tipoContatoList;
    }
    
    /*
     * =================================================
     * Passando parâmetro entre ManagedBeans - Exemplo A
     * =================================================
     * */    
    public String criarNovoContato(Pessoa p) {
        FacesUtil.setSessionAttribute(CONTATO_BEAN_PESSOA_TO_LOAD_EXAMPLE_A, p);
        return "/pages/contato/contato?faces-redirect=true";
    }
    
    
    /*
     * =================================================
     * Passando parâmetro entre ManagedBeans - Exemplo C
     * =================================================
     * */  
    public Integer getPessoaToLoadExampleC() {
        return pessoaToLoadExampleC;
    }

    public void setPessoaToLoadExampleC(Integer pessoaToLoadExampleC) {
        this.pessoaToLoadExampleC = pessoaToLoadExampleC;
    }    
    
    public void loadPessoaExampleC() {
        if (pessoaToLoadExampleC != null) {
            getEntity().setPessoa(pessoaService.findById(Integer.valueOf(pessoaToLoadExampleC)));
        }        
    }


}