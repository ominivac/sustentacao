package br.jus.tjrn.tjrndefaultarchetype.web;

import java.util.List;

import javax.inject.Inject;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.jus.tjrn.arq.jsf.controls.AbstractCrudBean;
import br.jus.tjrn.tjrndefaultarchetype.domain.Pessoa;
import br.jus.tjrn.tjrndefaultarchetype.service.PessoaService;

@Controller
@Scope("view")
public class PessoaBean extends AbstractCrudBean<Pessoa, PessoaService> {

    /*
     * Este atributo dataModel deve ser criado e utilizado em casos que o 
     * dataLazyModel não atende, como por exemplo no caso em que o componente
     * visual (p:datatable) vai executa a seleção de registros.
     */
    List<Pessoa> dataModel;
    List<Pessoa> dataModelSelected;
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Inject
    public PessoaBean(PessoaService service) {
        super(service);
    }

    /**
     * Método que pode ser utilizado para carregar dados de serviços auxiliares.
     */
    @Override
    public void onInit() {
    }

    public List<Pessoa> getDataModel() {
        return dataModel;
    }

    public void setDataModel(List<Pessoa> dataModel) {
        this.dataModel = dataModel;
    }
    
    public List<Pessoa> getDataModelSelected() {
        return dataModelSelected;
    }

    public void setDataModelSelected(List<Pessoa> dataModelSelected) {
        this.dataModelSelected = dataModelSelected;
        
        /*
         * Exibindo os registros selecionados. Dependendo do contexto, pode ser
         * mais apropriado criar um método só pra exibir essa mensagem.
         */
        if (dataModelSelected != null && dataModelSelected.size() > 0) {
            String nomes = "";
            for (Pessoa pessoa : dataModelSelected) {
                if (nomes.length() > 0) {
                    nomes += ", " + pessoa.getNome();
                } else {
                    nomes += pessoa.getNome();
                }
            }
            addInfo("Pessoas selecionadas: " + nomes);
        } else {
            addWarning("Nenhuuma pessoa selecionada");
        }

    }

    @Override
    public void search() {
        // TODO Auto-generated method stub
        super.search();
        setDataModel(getService().findByAttributes(getEntityToSearch()));
    }
    
}