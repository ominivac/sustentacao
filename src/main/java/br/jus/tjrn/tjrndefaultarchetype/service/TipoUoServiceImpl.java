package br.jus.tjrn.tjrndefaultarchetype.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.jus.tjrn.arq.security.spring.RoleGroup;
import br.jus.tjrn.arq.service.CrudServiceImpl;
import br.jus.tjrn.tjrndefaultarchetype.dao.TipoUoDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoUo;

@Service
@RoleGroup(name="TIPO_UO")
public class TipoUoServiceImpl extends CrudServiceImpl<TipoUo, TipoUoDao> implements TipoUoService{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
    @Inject
    public TipoUoServiceImpl(TipoUoDao dao) {
        super(dao);
    }

}
