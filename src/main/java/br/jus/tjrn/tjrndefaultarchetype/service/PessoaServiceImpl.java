package br.jus.tjrn.tjrndefaultarchetype.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.jus.tjrn.arq.security.spring.RoleGroup;
import br.jus.tjrn.arq.service.CrudServiceImpl;
import br.jus.tjrn.tjrndefaultarchetype.dao.PessoaDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.Pessoa;

@Service
@RoleGroup(name="PESSOA")
public class PessoaServiceImpl extends CrudServiceImpl<Pessoa, PessoaDao> implements PessoaService{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
    @Inject
    public PessoaServiceImpl(PessoaDao dao) {
        super(dao);
    }

}
