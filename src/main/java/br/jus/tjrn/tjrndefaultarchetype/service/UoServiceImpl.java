package br.jus.tjrn.tjrndefaultarchetype.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.jus.tjrn.arq.security.spring.RoleGroup;
import br.jus.tjrn.arq.service.CrudServiceImpl;
import br.jus.tjrn.tjrndefaultarchetype.dao.UoDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.Uo;

@Service
@RoleGroup(name="UO")
public class UoServiceImpl extends CrudServiceImpl<Uo, UoDao> implements UoService{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
    @Inject
    public UoServiceImpl(UoDao dao) {
        super(dao);
    }

}
