package br.jus.tjrn.tjrndefaultarchetype.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.jus.tjrn.arq.security.spring.RoleGroup;
import br.jus.tjrn.arq.service.CrudServiceImpl;
import br.jus.tjrn.tjrndefaultarchetype.dao.TipoContatoDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoContato;

@Service
@RoleGroup(name="TIPO_CONTATO")
public class TipoContatoServiceImpl extends CrudServiceImpl<TipoContato, TipoContatoDao> implements TipoContatoService{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
    @Inject
    public TipoContatoServiceImpl(TipoContatoDao dao) {
        super(dao);
    }

    @Override
    public List<TipoContato> findByNomeExistentePorPessoa(String nome, Integer pessoaId) {
        return dao.findByNomeExistentePorPessoa(nome, pessoaId);
    }

}
