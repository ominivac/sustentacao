package br.jus.tjrn.tjrndefaultarchetype.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import br.jus.tjrn.arq.security.spring.RoleGroup;
import br.jus.tjrn.arq.service.CrudServiceImpl;
import br.jus.tjrn.tjrndefaultarchetype.dao.ContatoDao;
import br.jus.tjrn.tjrndefaultarchetype.domain.Contato;

@Service
@RoleGroup(name="CONTATO")
public class ContatoServiceImpl extends CrudServiceImpl<Contato, ContatoDao> implements ContatoService{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;    
    
    @Inject
    public ContatoServiceImpl(ContatoDao dao) {
        super(dao);
    }

}
