package br.jus.tjrn.tjrndefaultarchetype.service;

import java.util.List;

import br.jus.tjrn.arq.service.CrudService;
import br.jus.tjrn.tjrndefaultarchetype.domain.TipoContato;

public interface TipoContatoService extends CrudService<TipoContato> {   
    
    public abstract List<TipoContato> findByNomeExistentePorPessoa(String nome, Integer pessoaId); 
    
}
