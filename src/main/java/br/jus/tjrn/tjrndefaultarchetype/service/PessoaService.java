package br.jus.tjrn.tjrndefaultarchetype.service;

import br.jus.tjrn.arq.service.CrudService;
import br.jus.tjrn.tjrndefaultarchetype.domain.Pessoa;

public interface PessoaService extends CrudService<Pessoa> {   
}
