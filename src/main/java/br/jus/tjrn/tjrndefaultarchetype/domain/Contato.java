package br.jus.tjrn.tjrndefaultarchetype.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.jus.tjrn.arq.persistence.Persistent;
import br.jus.tjrn.arq.persistence.PersistentUtil;

@Entity
@Table(name = "Contato")
public class Contato implements Persistent {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne()
    @NotNull(message = "O campo Pessoa não pode ser vazio.")
    @JoinColumn(name = "pessoa_id")
    private Pessoa pessoa;

    @ManyToOne()
    @NotNull(message = "O campo TipoContato não pode ser vazio.")
    @JoinColumn(name = "tipoContato_id")
    private TipoContato tipoContato;

    @NotNull(message = "O campo descricao não pode ser vazio.")
    @Length(min = 2, max = 50, message = "O campo descricao deve conter entre {min} e {max} caracteres.")
    @Column(name = "descricao", nullable = false)
    private String descricao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    @Transient
    public String getLabel() {
        return descricao;
    }

    @Override
    @Transient
    public String getEntityLabel() {
        return "Contato";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((descricao == null) ? 0 : descricao.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((pessoa == null) ? 0 : pessoa.hashCode());
        result = prime * result
                + ((tipoContato == null) ? 0 : tipoContato.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Contato other = (Contato) obj;
        if (PersistentUtil.hasSameId(this, other)) {
            return true;
        }
        if (descricao == null) {
            if (other.descricao != null)
                return false;
        } else if (!descricao.equals(other.descricao))
            return false;
        if (pessoa == null) {
            if (other.pessoa != null)
                return false;
        } else if (!pessoa.equals(other.pessoa))
            return false;
        if (tipoContato == null) {
            if (other.tipoContato != null)
                return false;
        } else if (!tipoContato.equals(other.tipoContato))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Contato [id=" + id + ", pessoa=" + pessoa + ", tipoContato="
                + tipoContato + ", descricao=" + descricao + "]";
    }

}
