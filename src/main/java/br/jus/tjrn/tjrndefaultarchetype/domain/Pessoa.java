package br.jus.tjrn.tjrndefaultarchetype.domain;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.jus.tjrn.arq.persistence.Persistent;
import br.jus.tjrn.arq.persistence.PersistentUtil;

@Entity
@Table(name = "Pessoa")
public class Pessoa implements Persistent {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "foto", nullable = true)
    @Lob
    private byte[] foto;

    @NotNull(message = "O campo nome não pode ser vazio.")
    @Length(min = 2, max = 100, message = "O campo nome deve conter entre {min} e {max} caracteres.")
    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "nomeAbreviado", nullable = true)
    private String nomeAbreviado;

    @NotNull(message = "O campo sexo não pode ser vazio.")
    @Length(min = 1, max = 1, message = "O campo sexo deve conter entre {min} e {max} caracteres.")
    @Column(name = "sexo", nullable = false)
    private String sexo;

    @Column(name = "cpf", nullable = true)
    private String cpf;

    @Column(name = "dataNascimento", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @NotNull(message = "O campo dataHoraUltimoRegistro não pode ser vazio.")
    @Column(name = "dataHoraUltimoRegistro", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataHoraUltimoRegistro;

    @NotNull(message = "O campo loginUsuarioUltimoRegistro não pode ser vazio.")
    @Length(min = 2, max = 20, message = "O campo loginUsuarioUltimoRegistro deve conter entre {min} e {max} caracteres.")
    @Column(name = "loginUsuarioUltimoRegistro", nullable = false)
    private String loginUsuarioUltimoRegistro;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNomeAbreviado() {
        return nomeAbreviado;
    }

    public void setNomeAbreviado(String nomeAbreviado) {
        this.nomeAbreviado = nomeAbreviado;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Date getDataHoraUltimoRegistro() {
        return dataHoraUltimoRegistro;
    }

    public void setDataHoraUltimoRegistro(Date dataHoraUltimoRegistro) {
        this.dataHoraUltimoRegistro = dataHoraUltimoRegistro;
    }

    public String getLoginUsuarioUltimoRegistro() {
        return loginUsuarioUltimoRegistro;
    }

    public void setLoginUsuarioUltimoRegistro(String loginUsuarioUltimoRegistro) {
        this.loginUsuarioUltimoRegistro = loginUsuarioUltimoRegistro;
    }

    @Override
    @Transient
    public String getLabel() {
        return nome;
    }

    @Override
    @Transient
    public String getEntityLabel() {
        return "Pessoa";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
        result = prime
                * result
                + ((dataHoraUltimoRegistro == null) ? 0
                        : dataHoraUltimoRegistro.hashCode());
        result = prime * result
                + ((dataNascimento == null) ? 0 : dataNascimento.hashCode());
        result = prime * result + Arrays.hashCode(foto);
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime
                * result
                + ((loginUsuarioUltimoRegistro == null) ? 0
                        : loginUsuarioUltimoRegistro.hashCode());
        result = prime * result + ((nome == null) ? 0 : nome.hashCode());
        result = prime * result
                + ((nomeAbreviado == null) ? 0 : nomeAbreviado.hashCode());
        result = prime * result + ((sexo == null) ? 0 : sexo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pessoa other = (Pessoa) obj;
        if (PersistentUtil.hasSameId(this, other)) {
            return true;
        }        
        if (cpf == null) {
            if (other.cpf != null)
                return false;
        } else if (!cpf.equals(other.cpf))
            return false;
        if (dataHoraUltimoRegistro == null) {
            if (other.dataHoraUltimoRegistro != null)
                return false;
        } else if (!dataHoraUltimoRegistro.equals(other.dataHoraUltimoRegistro))
            return false;
        if (dataNascimento == null) {
            if (other.dataNascimento != null)
                return false;
        } else if (!dataNascimento.equals(other.dataNascimento))
            return false;
        if (!Arrays.equals(foto, other.foto))
            return false;
        if (loginUsuarioUltimoRegistro == null) {
            if (other.loginUsuarioUltimoRegistro != null)
                return false;
        } else if (!loginUsuarioUltimoRegistro
                .equals(other.loginUsuarioUltimoRegistro))
            return false;
        if (nome == null) {
            if (other.nome != null)
                return false;
        } else if (!nome.equals(other.nome))
            return false;
        if (nomeAbreviado == null) {
            if (other.nomeAbreviado != null)
                return false;
        } else if (!nomeAbreviado.equals(other.nomeAbreviado))
            return false;
        if (sexo == null) {
            if (other.sexo != null)
                return false;
        } else if (!sexo.equals(other.sexo))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Pessoa [id=" + id + ", nome=" + nome + ", nomeAbreviado="
                + nomeAbreviado + ", sexo=" + sexo + ", cpf=" + cpf
                + ", dataNascimento=" + dataNascimento
                + ", dataHoraUltimoRegistro=" + dataHoraUltimoRegistro
                + ", loginUsuarioUltimoRegistro=" + loginUsuarioUltimoRegistro
                + "]";
    }

}
