package br.jus.tjrn.tjrndefaultarchetype.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.jus.tjrn.arq.persistence.Persistent;
import br.jus.tjrn.arq.persistence.PersistentUtil;

@Entity
@Table(name = "Uo")
public class Uo implements Persistent {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Integer id;

    @NotNull(message = "O campo nome não pode ser vazio.")
    @Length(min = 2, max = 100, message = "O campo nome deve conter entre {min} e {max} caracteres.")
    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "sigla", nullable = true)
    private String sigla;

    @NotNull(message = "O campo ativa não pode ser vazio.")
    @Column(name = "ativa", nullable = false)
    private Boolean ativa;

    @ManyToOne()
    @NotNull(message = "O campo TipoUo não pode ser vazio.")
    @JoinColumn(name = "tipoUo_id")
    private TipoUo tipoUo;

    @ManyToOne()
    @JoinColumn(name = "uoPai_id", nullable = true)
    private Uo uoPai;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

    public TipoUo getTipoUo() {
        return tipoUo;
    }

    public void setTipoUo(TipoUo tipoUo) {
        this.tipoUo = tipoUo;
    }

    public Uo getUoPai() {
        return uoPai;
    }

    public void setUoPai(Uo uoPai) {
        this.uoPai = uoPai;
    }

    @Override
    @Transient
    public String getLabel() {
        try {
            return nome.replace("{TipoUo}", tipoUo.getNome());
        } catch (Exception e) {
            return nome;
        }
    }

    @Override
    @Transient
    public String getEntityLabel() {
        return "Uo";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ativa == null) ? 0 : ativa.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((nome == null) ? 0 : nome.hashCode());
        result = prime * result + ((sigla == null) ? 0 : sigla.hashCode());
        result = prime * result + ((tipoUo == null) ? 0 : tipoUo.hashCode());
        result = prime * result + ((uoPai == null) ? 0 : uoPai.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Uo other = (Uo) obj;
        if (PersistentUtil.hasSameId(this, other)) {
            return true;
        }
        if (ativa == null) {
            if (other.ativa != null)
                return false;
        } else if (!ativa.equals(other.ativa))
            return false;
        if (nome == null) {
            if (other.nome != null)
                return false;
        } else if (!nome.equals(other.nome))
            return false;
        if (sigla == null) {
            if (other.sigla != null)
                return false;
        } else if (!sigla.equals(other.sigla))
            return false;
        if (tipoUo == null) {
            if (other.tipoUo != null)
                return false;
        } else if (!tipoUo.equals(other.tipoUo))
            return false;
        if (uoPai == null) {
            if (other.uoPai != null)
                return false;
        } else if (!uoPai.equals(other.uoPai))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Uo [id=" + id + ", nome=" + nome + ", sigla=" + sigla
                + ", ativa=" + ativa + ", tipoUo=" + tipoUo + ", uoPai="
                + uoPai + "]";
    }

}
