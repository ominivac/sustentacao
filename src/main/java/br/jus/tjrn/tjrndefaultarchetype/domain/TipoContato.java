package br.jus.tjrn.tjrndefaultarchetype.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import br.jus.tjrn.arq.persistence.Persistent;
import br.jus.tjrn.arq.persistence.PersistentUtil;

@Entity
@Table(name = "TipoContato")
public class TipoContato implements Persistent {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Integer id;

    @NotNull(message = "O campo nome não pode ser vazio.")
    @Length(min = 2, max = 50, message = "O campo nome deve conter entre {min} e {max} caracteres.")
    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "regex", nullable = true)
    private String regex;

    @NotNull(message = "O campo permiteMaisDeUmRegistroPorPessoa não pode ser vazio.")
    @Column(name = "permiteMaisDeUmRegistroPorPessoa", nullable = false)
    private Boolean permiteMaisDeUmRegistroPorPessoa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public Boolean getPermiteMaisDeUmRegistroPorPessoa() {
        return permiteMaisDeUmRegistroPorPessoa;
    }

    public void setPermiteMaisDeUmRegistroPorPessoa(
            Boolean permiteMaisDeUmRegistroPorPessoa) {
        this.permiteMaisDeUmRegistroPorPessoa = permiteMaisDeUmRegistroPorPessoa;
    }

    @Override
    @Transient
    public String getLabel() {
        return nome;
    }

    @Override
    @Transient
    public String getEntityLabel() {
        return "TipoContato";
    }



    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((nome == null) ? 0 : nome.hashCode());
        result = prime
                * result
                + ((permiteMaisDeUmRegistroPorPessoa == null) ? 0
                        : permiteMaisDeUmRegistroPorPessoa.hashCode());
        result = prime * result + ((regex == null) ? 0 : regex.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TipoContato other = (TipoContato) obj;
        if (PersistentUtil.hasSameId(this, other)) {
            return true;
        }
        if (nome == null) {
            if (other.nome != null)
                return false;
        } else if (!nome.equals(other.nome))
            return false;
        if (permiteMaisDeUmRegistroPorPessoa == null) {
            if (other.permiteMaisDeUmRegistroPorPessoa != null)
                return false;
        } else if (!permiteMaisDeUmRegistroPorPessoa
                .equals(other.permiteMaisDeUmRegistroPorPessoa))
            return false;
        if (regex == null) {
            if (other.regex != null)
                return false;
        } else if (!regex.equals(other.regex))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TipoContato [id=" + id + ", nome=" + nome + ", regex=" + regex
                + ", permiteMaisDeUmRegistroPorPessoa="
                + permiteMaisDeUmRegistroPorPessoa + "]";
    }

}
