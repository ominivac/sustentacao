from tomcat:9-jdk8
ARG JAR_FILE=target/*.war 
COPY ${JAR_FILE} /usr/local/tomcat/webapps/tjrn-default.war