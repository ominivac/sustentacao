echo "<<< Gerando o WAR da aplicacao >>>"
mvn clean package -Dmaven.test.skip=true -P container

echo "<<< Compilando a imagem da aplicacao >>>"
docker image build -f Dockerfile -t tjrn-default --no-cache .