﻿============
Fast Builder
============

=======================
Gerando um Novo Projeto
=======================
Pré-requisito: Projeto padrão instalado. Para isso basta:
- Baixar os fontes da versão desejada;
https://s-mexico.intrajus.tjrn/svn/projetopadrao/tjrn-default-archetype/tags/tjrn-default-archetype-{versao}
- Abrir o terminal e acessar a pasta:
tjrn-default-archetype-{versao}\src\others\
- Executar o comando abaixo. Ele executa pra você o procedimento explicado em http://s-mexico.intrajus.tjrn:4000/projects/projetopadrao/wiki/Default_Archetype.
archetype-install.bat


Pra criar um projeto do zero, acesse a quia "Novo Projeto", informa os dados necessários e clique em "Gerar Projeto". Uma vez feito isto, o Fast Builder irá criar uma pasta com o nome do seu projeto no local indicado em "Workspace".



===============================
Gerando os Fontes de Um Projeto
===============================
- Na guia "Conexões", informe os dados para a conexão com o banco de dados. A partir do banco de dados é que será gerado o código-fonte. Obs: Em uma versão futura pretendemos implementar a geração de código-fonte a partir das classes do domínio.

- Na guia "Tabelas do Banco" selecione as entidades desejadas. Dica: (1) Você pode gerar os fontes de todas as entidades de uma só vez, mas caso não tenha experiência com o Fast Builder recomendamos que você escolha uma entidade simples e gere os fontes somente dela. Depois que você deixar essa entidade funcionando na aplicação, aí você poderá partir para gerar, com mais confiança no que está fazendo, os fontes das demais entidades. (2) O Fast Builder identifica as FKs (chaves estrangeiras) existentes em cada tabela e já gera os cadastros levando me conta isso, logo se existir duas entidades ligadas (Ex: Cidade M > 1 Estado), selecione-as e mande executar o Fast Builder que ambos os CRUDs serão gerados com o devido relacionamento.

- Na guia "Templates" informe os templates de código-fonte que serão gerados para cada entidade selecionada na guia "Tabelas do Banco". Os templates por padrão ficam no local abaixo:
{myproject}\src\others\templatesfb

Para cada template você deverá indicar como os templates serão nomeados no momento da geração do código-fonte. Abaixo segue o padrão adotado atualmente:

Renomear utilizando entidade:
main.xhtm
Model.java

Concatenar entidade mais template:
Bean.java
Dao.java
DaoImpl.java
Service.java
ServiceImpl.java

Manter nome no template:
form.xhtml
list.xhtml

Dica: (1) Você não precisa gerar os fontes de todos os templates. Fica a seu critério. (2) Você pode gerar os fontes "n" vezes. O Fast Builder irá fazer a sobreescrita dos templates para cada entidade selecionada na guia "Tabelas do Banco". 


- Na guia "Processamento":
Informe o arquivo de propriedades que contém o nome dos pacotes do projeto. É com base nesse arquivos que o Fast Builder irá encaixar as classes em seus respectivos pacotes.
Ex: {myproject}\src\others\templatesfb\project.properties

Informe a pasta de saída. É nessa pasta que os fontes serão gerados. Informe a raiz do projeto, pois o Fast Builder automaticamente vai gerar os fontes e por dentro da pasta "src", nos devidos pacotes.
Ex: C:\TJRN_SDK_0.6.0.Final\Source\TJRN\{myproject}

Por fim, clique em "Gerar código-fonte".


============================================
Ajustar os Fontes Gerados Pelo Fast Builder
============================================
Uma vez gerado os fontes, acesse o Eclipse e faça os seguintes ajustes nos fontes:

- Pacote "br.jus.tjrn.{myproject}.domain"Ç
Para cada classe do domínio, gere os códigos fontes dos métodos hashCode(), equals() e toString(). Atente para a dica presente na própria classe para gerar o método toString(); 
Organize os imports (Ctrl + Shift + O) e formate os fontes (Ctrl + Shift + P);
Implemente o método getLabel().

- Pacote "br.jus.tjrn.myproject.service":
Nas implementações, ajuste o nome das permissões de acesso das entidades que tem o nome composto por mais de uma palavra.
Ex: Altere @RoleGroup(name="TIPOUO") para @RoleGroup(name="TIPO_UO")

- Pacote "br.jus.tjrn.myproject.web":
Organize os imports (Ctrl + Shift + O) e formate os fontes (Ctrl + Shift + P).

- Páginas web de cada entidade:
{myproject}/src/main/webapp/pages/tipouo/form.xhtml
{myproject}/src/main/webapp/pages/tipouo/list.xhtml

Selecione o fonte que está dentro da tag "<ui:composition>" e formate-o (Ctrl + Shift + F).

Ajuste o nome das permissões de acesso das entidades que tem o nome composto por mais de uma palavra.
Ex:
Mude <sec:authorize access="hasAnyRole('OP_TIPOUO_LER', 'OP_TIPOUO_ALTERAR', 'OP_TIPOUO_EXCLUIR')">
para <sec:authorize access="hasAnyRole('OP_TIPO_UO_LER', 'OP_TIPO_UO_ALTERAR', 'OP_TIPO_UO_EXCLUIR')">

Dica: Uma maneira simples e rápida é fazer uma busca geral (Ctrl + H, guia File Search) nos arquivos "xhtml". Faça a busca por "OP_TIPOUO_" e mande substituir por "OP_TIPO_UO_".

- Registre as permissões de acesso:
Inclua as devidas permissões de acesso no banco que será usado para autenticação. Caso esteja em ambiente de desenvolvimento e utilizando a autenticação em memória, você pode adicionar as permissões no arquivo abaixo para poder rodar e testar os fontes. 
{myproject}\src\main\webapp\WEB-INF\security-inmemory.xml

Obs: Lembre-se que antes de colocar a aplicação em produção, tais permissões devem ser adicionadas no banco de autenticação, que por padrão é o banco do Zeus.


- Crie a entrada no menu da aplicação:
{myproject}\src\main\webapp\includes\menu.xhtml


- Por fim, rode a aplicação, faça um teste básico e depois vá para a praia.  :)


FIM


