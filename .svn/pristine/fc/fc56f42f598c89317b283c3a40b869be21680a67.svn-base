<?xml version="1.0" encoding="UTF-8"?>
<!-- 
 * Copyright (c) 2011, Tribunal de Justiça do RN. 
 * Todos os direitos reservados. 
-->
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:context="http://www.springframework.org/schema/context"
    xmlns:jee="http://www.springframework.org/schema/jee"
    xmlns:p="http://www.springframework.org/schema/p"
    xmlns:sec="http://www.springframework.org/schema/security"
    xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
		http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context-3.1.xsd
		http://www.springframework.org/schema/jee http://www.springframework.org/schema/jee/spring-jee-3.1.xsd
		http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security-3.1.xsd">

    <sec:authentication-manager alias="authenticationManager">
        <sec:authentication-provider ref="ldapAuthProvider" />
    </sec:authentication-manager>

    <bean class="org.springframework.security.ldap.authentication.LdapAuthenticationProvider" id="ldapAuthProvider">
        <constructor-arg>
            <bean class="org.springframework.security.ldap.authentication.BindAuthenticator">
                <constructor-arg ref="ldapLocal" />
                <property name="userSearch">
                    <bean class="org.springframework.security.ldap.search.FilterBasedLdapUserSearch">
                        <constructor-arg value="" />
                        <constructor-arg value="(&amp;(objectClass=user)(userPrincipalName=${auth.ldap.userPrincipalName}))" />
                        <constructor-arg ref="ldapLocal" />
                    </bean>
                </property>
            </bean>
        </constructor-arg>
        <constructor-arg>
            <bean class="org.springframework.security.ldap.authentication.UserDetailsServiceLdapAuthoritiesPopulator">
                <constructor-arg>
                    <bean class="br.jus.tjrn.arq.security.spring.SystemOperationRoleJdbc">
                        <property name="dataSource" ref="dataSourceSecurity" />
                        <property name="enableGroups" value="false" />
                        <property name="enableAuthorities" value="true" />
                        <property name="systemCode" value="${app.code}" />
                        <property name="rolePrefix" value="" />

                        <!--
                            Se houver necessidade, você pode definir a consulta responsável pela
                            autenticação através da propriedade "usersByUsernameQuery".

                            Se houver necessidade, você pode definir a consulta responsável pela
                            autorização (ROLES) através da propriedade "authoritiesByUsernameQuery".

                            Para mais detalhes consultar a classe "SystemOperationRoleJdbc".

                            Obs: A arquitetura já oferece, por padrão, consultas para utilizar
                            o Zeus como "autorizador". Desta forma, caso vá utilizar o Zeus,
                            você poderá remover a configuração abaixo e assim usar a default.
                        -->

                        <property name="usersByUsernameQuery"
                            value=" SELECT  u.login, '' as password, 1 AS ativo
                                    FROM    Usuario u
                                    WHERE   u.login = ?
                                            AND 'TJRN_DEFAULT_ARCHETYPE' = ?
                                            AND u.ativo = 1" />

                        <property name="authoritiesByUsernameQuery"
                            value=" SELECT  u.login, o.codigo
                                    FROM    Usuario u
                                    INNER   JOIN UsuarioOperacao uo ON uo.usuario_id = u.id
                                    INNER   JOIN Operacao o ON o.id = uo.operacao_id    
                                    WHERE   u.login = ?
                                            AND 'TJRN_DEFAULT_ARCHETYPE' = ?
                                            AND u.ativo = 1" />
                    </bean>
                </constructor-arg>
            </bean>
        </constructor-arg>
        <property name="userDetailsContextMapper">
            <bean class="org.springframework.security.ldap.userdetails.InetOrgPersonContextMapper" />
        </property>
    </bean>
    <!-- Fim da configuração de autenticação em LDAP / autorização em banco -->


    <!-- Início da configuração de autenticação em LDAP / autorização em banco -->
    <sec:ldap-server id="ldapLocal" root="${auth.ldap.root}" url="ldap://${auth.ldap.host}:${auth.ldap.port}/${auth.ldap.root}"
        manager-dn="${auth.ldap.user}" manager-password="${auth.ldap.password}" />

</beans>
